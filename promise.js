const request = require('request');

class CustomPromise {
    static PENDING = 0;
    static FULFILLED = 1;
    static REJECTED = 2;
    constructor(callback) {
        if (typeof callback === 'function') {
            callback(data => {
                this.fulfill(data);
            }, err => this.reject(err), 0);
        } else {
            this.fulfill(callback);
        }
    }

    state = CustomPromise.PENDING;
    value = undefined;
    handlers = [];

    fulfill(value) {
        if (this.state === CustomPromise.FULFILLED) return;
        this.value = value;
        this.state = CustomPromise.FULFILLED;
        this.handlers.forEach(handler => setTimeout(() => this.handleState(handler)));
        delete this.handlers;
    }

    reject(value) {
        this.value = value;
        this.state = CustomPromise.REJECTED;
        this.handlers.forEach(handler => setTimeout(() => this.handleState(handler)));
        delete this.handlers;
    }

    resolve(data) {
        let p = new CustomPromise(data);
        return p;
    };

    handleState(handler) {
        switch (this.state) {
            case CustomPromise.FULFILLED:
                return handler.fullFilled(this.value);
            case CustomPromise.REJECTED:
                if (handler.rejected)
                    return handler.rejected(this.value);
                else return null
            case CustomPromise.PENDING:
            default:
                this.handlers.push(handler);
                return;
        }
    }

    thenAbleIt(fullFiller) {
        return (value) => {
            const unsafeP = fullFiller(value);
            if (typeof unsafeP === 'object' && typeof unsafeP.then === 'function') return unsafeP;
            return this.resolve(unsafeP);
        }
    }

    registerHandler(fullFilled, rejected) {

        if (this.state === CustomPromise.FULFILLED) {
            setTimeout(() => fullFilled(this.value))
        } else {
            this.handlers.push({
                fullFilled,
                rejected
            });
        }
    }


    then(fullFiller, rejector) {
        return new CustomPromise((resolve, reject) => {
            return this.handleState({
                fullFilled: value => {
                    if (typeof fullFiller === 'function') {
                        try {
                            return resolve(fullFiller(value))
                        } catch (err) {
                            return reject(err)
                        }
                    } else {
                        return resolve(value)
                    }
                },
                rejected: err => {
                    if (typeof rejector === 'function') {
                        try {
                            return resolve(rejector(err))
                        } catch (err) {
                            return reject(err);
                        }
                    } else {
                        return reject(err);
                    }
                }
            });

        })
    }
    catch (rejector) {
        return new CustomPromise((resolve, reject) => {
            return this.handleState({
                fullFilled: () => {},
                rejected: err => {
                    if (typeof rejector === 'function') {
                        try {
                            return resolve(rejector(err))
                        } catch (err) {
                            return reject(err);
                        }
                    } else {
                        return reject(err);
                    }
                }
            });

        })
    }


}



new CustomPromise((resolve, reject) => {
        request({
            url: 'https://buy-backend.whatauto.expert/api/v1/fixed-costs'
        }, (err, resp, body) => {

            // if (true) return reject(3231);

            const parsedBody = JSON.parse(body);
            resolve(parsedBody.length);

            console.log("\x1b[36m", 'promise request', parsedBody.length)
        });
    })
    .then((val) => {
        setTimeout(() => {
            console.log(val, 'i am then')
            console.log("\x1b[36m", 'last then timeout')
        }, 1000);
        return 3;
    })
    .then(console.log)
    .then(console.log)
    .catch(console.log);




new Promise((resolve, reject) => {
        request({
            url: 'https://buy-backend.whatauto.expert/api/v1/fixed-costs'
        }, (err, resp, body) => {

            // if (true) return reject(3231);

            const parsedBody = JSON.parse(body);
            resolve(parsedBody.length);

            console.log("\x1b[36m", 'promise request', parsedBody.length)
        });
    })
    .then((val) => {
        setTimeout(() => {
            console.log(val, 'i am then')
            console.log("\x1b[36m", 'last then timeout')
        }, 1000);
        return 3;
    })
    .then(console.log)
    .then(console.log)
    .catch(console.log);